La variable en programmation
=============================

Une ``variable`` est définie par un nom. Ce nom peut contenir:

-   des lettres de l'alphabet en minuscule ou majuscule éventuellement mélangées;
-   une combinaison alphanumérique c'est à dire un mélange de lettres et de chiffres;
-   le souligné ou tiret du bas ``_``.

Voici quelques exemples de noms de variables:

-   Pour représenter un nombre : ``i``, ``n``, ``x``, ``y``.
-   Pour représenter une chaine de caractères : ``mot``, ``nom``, ``texte``.

.. note::

    #.  Il est fortement conseillé de nommer sa variable en tenant compte des données qu'elle contient. Par exemple, pour une température , on utilise une variable nommée ``temperature`` ou ``temp`` ou ``tp``.
    #.  Lorsque deux variables représentent des grandeurs similaires, on choisit des noms proches mais qui se différencient par un chiffre placé à la fin : ``mot1`` et ``mot2`` ou ``x_1`` et ``x_2``.

La variable en python
----------------------

On crée une variable en Python en écrivant une instruction qui contient le ``nom`` de la variable suivi du signe ``=`` suivi d'une ``valeur initiale``.

.. code:: python

    variable = valeur initiale


Par exemple, on crée les variables ``a`` et ``b`` en affectant respectivement les valeurs 4 et 5.

>>> a = 4
>>> b = 5

.. note::
    
    Attribuer une valeur à une variable est une **affectation**. 

Les types de variables
-----------------------

Une variable possède un **type** qui correspond au type de la donnée qu'elle contient.
Une variable a un type unique. Voici quelques exemples de types de variables.

-   Si on attribue un nombre entier à une variable alors elle est de type ``int`` (abréviation de integer).

    >>> n = 8
    >>> type(n)
    <class 'int'>

-   Si on attribue un nombre à virgule à une variable, alors elle est de type ``float``.

    >>> a = 1.36
    >>> type(a)
    <class 'float'>

-   Si on attribue une chaine de caractères qui se note entre des quotes, alors elle est de type ``str`` (abréviation de string).

    >>> mot = 'bonjour'
    >>> type(mot)
    <class 'str'>

.. caution::

    Les **quotes** se notent soit avec l'apostrophe (single quote), soit avec les guillemets (double quote). Par exemple, la chaine de valeur "bonjour" se note ``'bonjour'`` ou ``"bonjour"``.


Valeurs d'une variable
-----------------------

Une variable créée dans un programme peut changer de valeur (d'où son nom). Voici différentes situations conduisant à un changement de valeur.

#.  Une variable peut changer de valeur en lui affectant une nouvelle valeur. Par exemple, une variable ``x`` peut être initialisée à 0 puis modifiée avec la valeur 5.

    >>> x = 0
    >>> x = 5

#.  Une variable peut changer de valeur en tenant compte de sa valeur actuelle. Par exemple, une variable ``x`` est initialisée avec la valeur 3, puis modifiée en lui ajoutant la valeur 2. On écrit alors l'instruction:

    >>> x = 3
    >>> x = x + 2
    >>> x
    5

    Le langage procède de droite à gauche:

    -   il calcule d'abord la valeur ``x+2`` avec la valeur actuelle de la variable ``x``;
    -   ensuite il affecte la nouvelle valeur à la variable ``x``.

#.  En changeant de valeur, une variable peut changer de type. Par exemple, une variable ``x`` initialisée à 0 est de type ``int``. Si on divise la valeur de la variable ``x`` par le nombre 2, le type de la variable ``x`` est alors ``float``.

    >>> x = 0
    >>> x = x / 2
    >>> x
    0.0
    >>> type(x)
    <class 'float'>


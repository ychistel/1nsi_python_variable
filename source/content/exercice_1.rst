Exercices : variables et types
===============================

.. exercice::

    #.  Quelle est la valeur finale de la variable ``a`` si on exécute les instructions suivantes ?

        >>> a = 3
        >>> a = a*2    
        >>> a = a+2    
        >>> a

    #.  Quelle est la valeur finale des variables ``a`` et ``b`` si on exécute les instructions suivantes ?

        >>> a = 2
        >>> b = a*a
        >>> a = a*b
        >>> b = b*a
        >>> b

    #.  Quelle est la valeur finale des variables ``a``, ``b`` et ``c`` si on exécute les instructions suivantes ?

        >>> a = 7
        >>> b = a*2
        >>> a = b
        >>> c = a/2
        >>> a = c

    #.  Vérifier vos réponses en saisissant ces instructions dans l'interpréteur IDLE Python.

.. exercice::

    On utilise le logiciel **Thonny** pour cet exercice.
    
    On considère la variable ``a`` de valeur 15 et la variable ``b`` de valeur 4.

    #.  La variable ``f`` est définie par l'instruction:

        >>> f = a / b

        Quelle est la valeur de la variable ``f``? Quel est son type ?

    #.  La variable ``q`` est définie par l'instruction:
    
        >>> q = a // b 
        
        Quelle est la valeur de la variable ``q``? Quel est le type de la variable ``q`` ?

    #.  La variable ``r`` est définie par l'instruction:
    
        >>> r = a % b
        
        Quelle est la valeur de la variable ``r``? Quel est le type de la variable ``r`` ?

.. exercice::

    #.  Écrire des instructions Python pour réaliser les actions suivantes:

        -   créer la variable ``x`` contenant la chaine de caractères ``informatique``;
        -   créer la variable ``y`` contenant la chaine de caractères ``numérique``;
        -   échanger les contenus des variables ``x`` et ``y`` en utilisant que des variables. On ne réattribue pas les mots ``informatique`` et ``numérique`` aux variables.

    #.  Proposer des instructions en Python pour:

        -   créer 3 variables ``x``, ``y`` et ``z`` contenant respectivement  les mots ``science``, ``informatique`` et ``numérique``;
        -   échanger les valeurs pour que les variables ``x``, ``y`` et ``z`` contiennent respectivement  les mots ``numérique``, ``science`` et ``informatique``.

.. exercice::

    Dans le monde des shadoks, il n'y a que 4 mots : GA, BU, ZO et MEU. Tous les mots de leur langage s'obtiennent par combinaison de ces mots.

    .. figure:: ../img/monde_des_shadoks.jpg
        :width: 360
        :align: center

    On considère les variables ``a``, ``b``, ``c`` et ``d`` de valeurs respectives ``ga``, ``bu``, ``zo`` et ``meu``.

    .. note::

        #.  Les chaines de caractères sont **concaténables**. Cela signifie qu'on peut accoller 2 chaines de caractères ensemble. Pour réaliser une **concaténation**, on utilise le signe d'addition ``+``.

            >>> 'a' + 'b'
            'ab'

        #.  Comme en mathématiques, lorsqu'on additionne plusieurs fois le même nombre, on peut utiliser la multiplication.

            >>> 'a'*3
            'aaa'

    Donner les instructions Python qui permettent en utilisant les variables ``a``, ``b``, ``c`` et ``d`` d'obtenir:

    #.  le mot ``meuga``
    #.  le mot ``zobuga``
    #.  le bébé qui dit ses premiers mots ``zozozozozo``
    #.  le shadock mécontent qui dit ``zobuzobuzobu``
    #.  la phrase ``meumeu gaga``
    #.  la pensée du jour exclamée ``zo bumeu gaga, ga meubu zozo!``

.. exercice::

    On considère les variables suivantes:

    -   ``n`` de type ``int`` (nombre entier) initialisée à 150.
    -   ``x`` de type ``float`` qui vaut ``5.0``. 

    #.  Écrire une instruction Python qui augmente la valeur de la variable ``n`` de 20.
    #.  Écrire une instruction Python qui crée une variable ``p`` dont la valeur est celle de la variable ``n`` divisée par 2.
    #.  Écrire une instruction Python qui modifie la valeur de la variable ``x`` en l'élevant au carré.
    #.  Quels sont les types des variables ``n``, ``p`` et ``x`` après avoir exécuté ces instructions ?
    #.  Écrire des instructions Python qui renvoie le type de chaque variable.

.. exercice::

    L'aire et le périmètre d'un rectangle se calcule avec les formules suivantes:

    -   Aire du rectangle : ``Largeur x longueur``
    -   Périmètre du rectangle : ``2 x (Largeur + longueur)``

    On va écrire un programme qui calcule l'aire et le périmètre d'un rectangle.

    #.  Créer les variables ``largeur`` et ``longueur`` de valeurs initiales égales à ``5`` et ``12``.
    #.  Créer une variable ``perimetre`` qui calcule le périmètre du rectangle.
    #.  Créer une variable ``aire`` qui calcule l'aire du rectangle.
    #.  Exécuter votre programme et vérifier les valeurs affichées.
    #.  Modifier les valeurs de la largueur et de la longueur du rectangle puis exécuter votre programme. Vérifier que l'affichage est conforme aux mesures données.
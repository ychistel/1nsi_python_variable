Le langage Python
===================

**Python** est un langage inventé par **Guido Von Rossum** [#]_. C'est un un langage interprété, ce qui signifie qu'à chaque exécution du programme écrit en Python, celui-ci est converti en binaire puis exécuté. 

L'exécution d'un programme Python se déroule en trois temps:

#.  Un programme Python est rédigé dans un éditeur.
#.  Le programme est exécuté par l'interpréteur.
#.  L'interpréteur affiche le résultat du programme.

Il existe différents logiciels pour créer un programme Python.

IDLE Python
--------------

Le site `python.org`_ propose de la documentation sur le langage, les évolutions du projet autour de Python et un espace de téléchargement qui contient différentes versions du logiciel selon son système d'exploitation.

Après téléchargement et installation de Python, on peut lancer le programme ``IDLE Python``. 

La fenêtre qui s'ouvre est l'**interpréteur** Python. On reconnaît facilement l'interpréteur car la ligne commence toujours par ``>>>``.

.. image:: ../img/idle_python.png
    :align: center
    :width: 600px
    :class: margin-bottom-16

**L'éditeur** de python est accessible par le menu de l'interpréteur ``File -> New File (Ctrl+n)`` qui affiche une deuxième fenêtre. C'est l'éditeur et c'est dans celle-ci qu'on saisit les programmes. 

.. image:: ../img/editeur_python.png
    :align: center
    :width: 600px
    :class: margin-bottom-16

Depuis l'éditeur, on exécute le programme avec le menu ``Run -> Run Module``  ou par un appui sur la touche ``F5`` du clavier après avoir enregistré le programme dans un fichier de son espace de travail.

Dans l'interpréteur, le programme est exécuté puis celui-ci affiche le résultat du programme ou les erreurs qu'il a rencontré dans le programme.

Notebook CAPYTALE
----------------

Un **notebook** est une application web qui permet de faire de la programmation Python dans un navigateur web comme firefox ou chrome.

La page web propose des cellules pour écrire ses programmes. Chaque cellule est divisée en 2 parties :

-   Une partie gauche contenant le mot ``Entrée`` suivi d'un numéro
-   Une seconde partie qui est un éditeur de code.

Un bouton ``Exécuter`` placé dans la barre de menu permet d'exécuter le code Python de la cellule active. Le résultat du programme est affiché juste en dessous de la cellule qui contient le code.

.. image:: ../img/notebook_jupyter.png
    :align: center
    :width: 600px
    :class: margin-bottom-16

L'ENT Normandie propose l'application web CAPYTALE qui permet de créer des **notebook**.

.. figure:: ../img/capytale_notebook.svg
    :align: center
    :width: 320

Logiciel Thonny
---------------

**Thonny** est un logiciel très léger qui embarque sa propre version de Python.

.. image:: ../img/editeur_thonny.png
    :align: center
    :width: 600px
    :class: margin-bottom-16

La fenêtre se décompose en plusieurs parties:

-   La partie **Édition** en haut qui permet de saisir son programme;
-   La partie **Console** est l'interpréteur qui exécute et affiche le résultat du programme;
-   La partie **Variables** qui affiche les valeurs des variables;
-   La partie **Assistant** qui affiche des conseils pour améliorer son programme.

Le logiciel **Thonny** est disponible en téléchargement sur le site `thonny.org`_.

.. [#] Page wikipedia de `Guido Van Rossum`_
.. _`Guido Van Rossum`: https://fr.wikipedia.org/wiki/Guido_van_Rossum
.. _`python.org`: https://python.org/
.. _`thonny.org`: https://thonny.org/
TP : afficher et saisir des données
====================================

Un programme contient :

-   des données affectées à des variables;
-   des instructions qui utilisent ces variables et les modifient.

Une fois exécuté, l'interpréteur ou la console Python n'affiche rien sauf si on lui demande la valeur d'une variable en particulier. De plus, le programme s'exécute avec les mêmes valeurs de variables sauf en changeant les valeurs dans le programme.

Il existe des fonctions Python qui permettent : 

-   d'afficher des informations et des valeurs pendant l'exécution du programme;
-   de saisir des valeurs à attribuer aux variables du programme.

On va réaliser des affichages et des saisies en reprenant le programme de calcul de l'aire et du périmètre d'un rectangle redonné ci-après :

.. code::

    longueur = 5
    largeur = 12
    perimetre = (longueur + largeur)*2
    aire = longueur * largeur


Créer un affichage dans la console
-------------------------------------

La fonction Python ``print()`` a pour role d'afficher dans l'interpréteur ou la console la valeur indiquée entre parenthèses. On peut afficher :

-   la valeur des variables, 
-   des messages d'information,
-   des messages d'information avec les valeurs des variables.

Dans l'exemple suivant, le programme crée 2 variables ``a`` et ``b`` puis affiche successivement, une information de début de programme, la valeur de la variable ``a``, la valeur de la variable ``b`` puis une information de fin de programme.

.. pyscript::

    a = 5
    b = 2
    print("début du programme")
    print(a)
    print(b)
    print("fin du programme")

.. rubric:: Travail à réaliser

#.  Ajouter en début de programme un affichage du message "Calcul du périmètre et de l'aire du rectangle".
#.  Ajouter une instruction qui affiche les valeurs des variables ``longueur`` et ``largeur``.
#.  La fonction ``print`` peut afficher un message en y insérant la valeur d'une variable. Pour cela, on utilise une chaine de caractères particulière appelée ``f-string``. La syntaxe est la suivante:

    .. pyscript::

        a = 10
        print(f"La variable a vaut {a}")
    
    a.  Réaliser un affichage du périmètre du rectangle avec sa valeur calculée.
    b.  Réaliser un affichage de l'aire du rectangle avec sa valeur calculée.

Saisir des valeurs dans la console
-------------------------------------

La fonction Python ``input()`` a pour role de mettre en pause le programme pour permettre à un utilisateur de saisir une valeur pour une variable. On guide l'utilisateur en ajoutant un message indiquant la variable pour laquelle il doit donner une valeur. La synrtaxe générale est:

.. code::

    variable = input("message pour guider l'utilisateur")

Dans l'exemple suivant, le programme demande la saisie de 2 valeurs à affecter aux variables ``a`` et ``b``.

.. code::

    a = input("Valeur de la variable a : ")
    b = int(input("Valeur de la variable b : "))
    print(f"La variable a vaut {a}")
    print(f"la variable b vaut {b}")
    print("fin du programme")

.. Attention::

    La fonction ``input()`` affecte à la variable la valeur saisie sous forme d'une chaine de caratères. Pour les nombres, il faut procéder à un changement de type avec la fonction ``int()``.

.. rubric:: Travail à réaliser

#.  Ajouter une saisie pour la longueur du rectangle.
#.  Ajouter une saisie pour la largeur du rectangle.
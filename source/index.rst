Les variables en python
========================

Un programme est une suite d'instructions écrites dans un langage qui a pour but de manipuler des données. Ces données sont utilisées pour effectuer des calculs, créer des affichages, pour être modifiées, etc.

Les données doivent être stockées en mémoire et être accessibles par le programme. Pour cela, on utilise des **variables** qui sont finalement des espaces de la mémoire à qui on attribue un nom : le nom de la variable.

Dans cette partie, nous allons avec le langage de programmation Python écrire des suites d'instructions qui manipulent des données stockées dans des variables.

.. toctree::
   :maxdepth: 1
   :hidden:
   
   content/editeur.rst
   content/variable.rst
   content/exercice_1.rst
   content/tp_saisir_afficher.rst
   content/liste_tuple.rst
   content/exercice_2.rst

